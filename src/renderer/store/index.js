import Vue from 'vue'
import Vuex from 'vuex'

import { createPersistedState } from 'vuex-electron'
Vue.use(Vuex)

export default new Vuex.Store({
  plugins: [
    createPersistedState()
    // createSharedMutations()
  ],
  strict: process.env.NODE_ENV !== 'production',
  state: {
    K: 0,
    A: 0,
    lab2: 0,
    MatrixA: [],
    MatrixK: [],
    NameOfA: [],
    NameOfK: [],
    Vector: [],
    Weight: []
  },
  mutations: {
    addValueA: (state, value) => {
      state.A = Number(value)
    },
    addValueK: (state, value) => {
      state.K = Number(value)
    },
    incrementLab2: (state, value) => {
      state.lab2 = value
    },
    setMatrix: (state, value) => {
      if (value.getVar === 'A') {
        state.MatrixA = value.Matrix
      } else if (value.getVar === 'K') {
        state.MatrixK = value.Matrix
      } else if (value.getVar === 'vector') {
        state.Vector = value.Matrix
      } else if (value.getVar === 'weight') {
        state.Weight = value.Matrix
      }
    },
    SaveNameOfMatrix: (state, value) => {
      state.NameOfA = value.A
      state.NameOfK = value.K
    }
  },
  actions: {
    addValueFirst ({commit}, payload) {
      commit('addValueK', payload.K)
      commit('addValueA', payload.A)
    },
    incrementLab2 ({commit}, payload) {
      commit('incrementLab2', payload)
    },
    Matrix ({commit}, payload) {
      commit('setMatrix', payload)
    },
    SaveNameOfMatrix ({commit}, payload) {
      commit('SaveNameOfMatrix', payload)
    }
  },
  getters: {
    first (state) {
      return {A: state.A, K: state.K}
    },
    lab2 (state) {
      return state.lab2
    },
    getMatrix (state) {
      return {A: state.MatrixA, K: state.MatrixK, Vector: state.Vector, Weight: state.Weight}
    },
    NameOfMatrix (state) {
      return {A: state.NameOfA, K: state.NameOfK}
    }
  }
})
