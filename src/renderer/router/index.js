import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'lab2',
      component: require('@/components/lab2').default
    },
    {
      path: '/lab3',
      name: 'lab3',
      component: require('@/components/lab3').default
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
